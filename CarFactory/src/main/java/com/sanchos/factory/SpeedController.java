package com.sanchos.factory;

import com.sanchos.factory.model.Dealer;
import com.sanchos.factory.model.Supplier;

import javax.swing.*;
// класс для создания окна с ползунками
public class SpeedController extends JFrame {
    private ResourceController controller;

    public SpeedController(ResourceController controller) {
        super("Speed control");
        this.controller = controller;
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JLabel label1 = new JLabel("Accessory");
        JLabel label2 = new JLabel("Engine");
        JLabel label3 = new JLabel("Body");
        JLabel label4 = new JLabel("Dealers");

        JSlider slideAccessory = new JSlider(SwingConstants.HORIZONTAL,1,10,3);
        slideAccessory.setMajorTickSpacing(1);
        slideAccessory.setPaintTicks(true);
        slideAccessory.setPaintLabels(true);
        slideAccessory.addChangeListener(changeEvent -> {
            int period = 1000 * (11 - ((JSlider) changeEvent.getSource()).getValue());
            for (Supplier supplier : controller.getAccessorySuppliers()){
                supplier.setPeriodSupply(period);
            }
        });

        JSlider slideEngine = new JSlider(SwingConstants.HORIZONTAL,1,10,3);
        slideEngine.setMajorTickSpacing(1);
        slideEngine.setPaintTicks(true);
        slideEngine.setPaintLabels(true);
        slideEngine.addChangeListener(changeEvent -> {
            int period = 1000 * (11 - ((JSlider) changeEvent.getSource()).getValue());
            for (Supplier supplier : controller.getEngineSuppliers()){
                supplier.setPeriodSupply(period);
            }
        });

        JSlider slideBody = new JSlider(SwingConstants.HORIZONTAL,1,10,3);
        slideBody.setMajorTickSpacing(1);
        slideBody.setPaintTicks(true);
        slideBody.setPaintLabels(true);
        slideBody.addChangeListener(changeEvent -> {
            int period = 1000 * (11 - ((JSlider) changeEvent.getSource()).getValue());
            for (Supplier supplier : controller.getBodySuppliers()){
                supplier.setPeriodSupply(period);
            }
        });

        JSlider slideDealer = new JSlider(SwingConstants.HORIZONTAL,1,10,2);
        slideDealer.setMajorTickSpacing(1);
        slideDealer.setPaintTicks(true);
        slideDealer.setPaintLabels(true);
        slideDealer.addChangeListener(changeEvent -> {
            int period = 1000 * (11 - ((JSlider) changeEvent.getSource()).getValue());
            for (Dealer dealer : controller.getDealers()){
                dealer.setPeriodRequest(period);
            }
        });


        JPanel contents = new JPanel();
        contents.add(label1);
        contents.add(slideAccessory);
        contents.add(label2);
        contents.add(slideEngine);
        contents.add(label3);
        contents.add(slideBody);
        contents.add(label4);
        contents.add(slideDealer);
        getContentPane().add(contents);
        setSize(283, 230);
        setResizable(false);
        setVisible(true);
    }
}


