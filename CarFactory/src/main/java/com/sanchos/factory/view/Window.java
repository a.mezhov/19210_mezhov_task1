package com.sanchos.factory.view;

import javax.swing.*;

public class Window extends JFrame {
    public static final int WINDOW_WIDTH = 1280;
    public static final int WINDOW_HEIGHT = 512;

    public Window(Canvas canvas) {
        setTitle("Factory");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT + 25);
        setLocationRelativeTo(null);
        setResizable(false);

        ImageIcon icon = new ImageIcon("assets/icon.png"); // миниатюра в окне
        setIconImage(icon.getImage());

        add(canvas);

        setVisible(true);
    }
}
