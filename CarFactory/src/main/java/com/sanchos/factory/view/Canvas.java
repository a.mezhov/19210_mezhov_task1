package com.sanchos.factory.view;

import com.sanchos.factory.ResourceController;

import javax.swing.*;
import java.awt.*;
// класс отрисовки картинки в окне
public class Canvas extends JPanel {
    private final ResourceController controller;
    private final Font font = new Font("Times New Roman", Font.PLAIN, 18);

    public Canvas(ResourceController controller) {
        this.controller = controller;
        setFocusable(true);
    }

    @Override
    public void paint(Graphics g) {
        String string;
        g.setFont(font);
        g.setColor(Color.darkGray);

        g.drawImage(new ImageIcon("assets/factory.png").getImage(), 0, 0, null);

        int max = controller.getAccessoryStorage().getCapacity();
        int cur = controller.getAccessoryStorage().getNumberItems();
        int period = controller.getAccessorySuppliers().get(0).getPeriodSupply();
        string = "[" + cur + "/" + max + "] Period: " + period;
        g.drawString(string, 40, 55);

        max = controller.getEngineStorage().getCapacity();
        cur = controller.getEngineStorage().getNumberItems();
        period = controller.getEngineSuppliers().get(0).getPeriodSupply();
        string = "[" + Math.min(cur, max) + "/" + max + "] Period: " + period;
        g.drawString(string, 40, 230);

        max = controller.getBodyStorage().getCapacity();
        cur = controller.getBodyStorage().getNumberItems();
        period = controller.getBodySuppliers().get(0).getPeriodSupply();
        string = "[" + Math.min(cur, max) + "/" + max + "] Period: " + period;
        g.drawString(string, 40, 395);

        cur = controller.getCarStorageManager().getSoldCarCounter();
        string = "Total made: " + cur;
        g.drawString(string, 425, 150);

        max = controller.getCarStorage().getCapacity();
        cur = controller.getCarStorage().getNumberItems();
        string = "[" + Math.min(cur, max) + "/" + max + "]";
        g.drawString(string, 790, 55);

        period = controller.getDealers().get(0).getPeriodRequest();
        string = "Period: " + period;
        g.drawString(string, 1110, 55);

        Toolkit.getDefaultToolkit().sync();
    }

    public void update() {
        repaint();
    }

}
