package com.sanchos.factory;

import com.sanchos.factory.model.*;
import com.sanchos.factory.view.Canvas;
import com.sanchos.factory.view.Window;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.Executor;

public class ResourceController implements ActionListener {
    private Window window;
    private Canvas canvas;
    private final Timer timer = new Timer(20,this);
    private SpeedController speedController;

    private final ThreadGroup threadGroup;
    private final Executor executor;
    private CarFactory mechanics;
    private CarStorageManager carStorageManager;

    private ArrayList<Supplier> accessorySuppliers;
    private ArrayList<Supplier> bodySuppliers;
    private ArrayList<Supplier> engineSuppliers;
    private ArrayList<Dealer> dealers;

    private Storage<Detail> accessoryStorage;
    private Storage<Detail> bodyStorage;
    private Storage<Detail> engineStorage;
    private Storage<Car> carStorage;

    private boolean log;

    public ResourceController() {
        this.speedController = new SpeedController(this);

        this.threadGroup = new ThreadGroup("members"); // Создает новую группу потоков. Родителем этой новой группы является группа потоков текущего запущенного потока.
        this.executor = (runnable) -> new Thread(threadGroup, runnable).start(); // Выделяет новый объект потока. Автоматически генерируемые имена имеют вид "Поток-"+n, где n-целое число.
        Properties properties = new Properties(); // Создает пустой список свойств без значений по умолчанию.

        try (InputStream in = getClass().getClassLoader().getResourceAsStream("number_participants.properties")) {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        initStorage(
                Integer.parseInt(properties.getProperty("StorageBodySize")),
                Integer.parseInt(properties.getProperty("StorageEngineSize")),
                Integer.parseInt(properties.getProperty("StorageAccessorySize")),
                Integer.parseInt(properties.getProperty("StorageCarSize"))
        );
        initSuppliers(
                Integer.parseInt(properties.getProperty("EngineSuppliers")),
                Integer.parseInt(properties.getProperty("BodySuppliers")),
                Integer.parseInt(properties.getProperty("AccessorySuppliers"))
        );

        mechanics = new CarFactory(
                Integer.parseInt(properties.getProperty("Mechanics")),
                accessoryStorage,
                bodyStorage,
                engineStorage,
                carStorage
        );
        log = Boolean.parseBoolean(properties.getProperty("Log"));
        carStorageManager = new CarStorageManager(mechanics, carStorage, log);
        initDealers(Integer.parseInt(properties.getProperty("Dealers")));

        this.canvas = new Canvas(this);
        this.window = new Window(canvas);
    }
    // инициализируем склады
    public void initStorage(int bodyCapacity, int engineCapacity, int accessoryCapacity, int carCapacity) {
        accessoryStorage = new Storage<>(accessoryCapacity);
        bodyStorage = new Storage<>(bodyCapacity);
        engineStorage = new Storage<>(engineCapacity);
        carStorage = new Storage<>(carCapacity);
    }
    // инициализируем поставщиков
    public void initSuppliers(int qtyEngineSuppliers, int qtyBodySuppliers, int qtyAccessorySuppliers) {
        engineSuppliers = new ArrayList<>(qtyEngineSuppliers);
        bodySuppliers = new ArrayList<>(qtyBodySuppliers);
        accessorySuppliers = new ArrayList<>(qtyAccessorySuppliers);
        // добавляем поставщиков
        for (int i = 0; i < qtyEngineSuppliers; ++i) {
            Supplier supplier = new Supplier(TypesDetail.ENGINE, engineStorage);
            engineSuppliers.add(supplier);
        }
        for (int i = 0; i < qtyBodySuppliers; ++i) {
            Supplier supplier = new Supplier(TypesDetail.BODY, bodyStorage);
            bodySuppliers.add(supplier);
        }
        for (int i = 0; i < qtyAccessorySuppliers; ++i) {
            Supplier supplier = new Supplier(TypesDetail.ACCESSORY, accessoryStorage);
            accessorySuppliers.add(supplier);
        }
    }
    // инициализируем дилеров
    public void initDealers(int qtyDealers) {
        dealers = new ArrayList<>(qtyDealers);

        for (int i = 0; i < qtyDealers; ++i) {
            Dealer dealer = new Dealer(carStorage, carStorageManager);
            dealers.add(dealer);
        }
    }

    public void start() {
        for (Supplier supplier : accessorySuppliers) {
            executor.execute(supplier);
        }
        for (Supplier supplier : engineSuppliers) {
            executor.execute(supplier);
        }
        for (Supplier supplier : bodySuppliers) {
            executor.execute(supplier);
        }
        for (Dealer dealer : dealers) {
            executor.execute(dealer);
        }

        timer.start();
    }

    public void stop() {
        mechanics.stop();
        threadGroup.interrupt();
        timer.stop();
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        carStorageManager.fillMinimum();
        canvas.update();
    }

    public CarStorageManager getCarStorageManager() {
        return carStorageManager;
    }

    public ArrayList<Supplier> getAccessorySuppliers() {
        return accessorySuppliers;
    }

    public ArrayList<Supplier> getBodySuppliers() {
        return bodySuppliers;
    }

    public ArrayList<Supplier> getEngineSuppliers() { return engineSuppliers; }

    public ArrayList<Dealer> getDealers() {
        return dealers;
    }

    public Storage<Detail> getAccessoryStorage() {
        return accessoryStorage;
    }

    public Storage<Detail> getBodyStorage() {
        return bodyStorage;
    }

    public Storage<Detail> getEngineStorage() {
        return engineStorage;
    }

    public Storage<Car> getCarStorage() {
        return carStorage;
    }

}
