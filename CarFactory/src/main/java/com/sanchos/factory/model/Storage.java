package com.sanchos.factory.model;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.atomic.AtomicLong;

public class Storage<T extends Identifiable> {
    private final Deque<T> item = new ArrayDeque<>();
    private final AtomicLong idGenerator = new AtomicLong();
    private final int capacity;

    public Storage(int capacity) {
        this.capacity = capacity;
    }

    public synchronized void addItem(T item) throws InterruptedException {
        while (this.item.size() == capacity){ // пока склад заполнен, ждём
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.item.size() == 0) { // когда склад пустой, то "пробуждаем" спящих поставщиков
            notify();
        }

        item.setId(idGenerator.incrementAndGet());
        this.item.push(item); // пушим элемент в очередь
    }

    public synchronized T getItem() throws InterruptedException {
        while (item.isEmpty()){ // пока очередь пуста, ждём
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.item.size() == capacity) { // если очередь заполнена, то "пробуждаем" рабочих
            notify();
        }
        return item.pop();
    }

    public int getNumberItems() {
        return item.size();
    }

    public int getCapacity() {
        return capacity;
    }
}
