package com.sanchos.factory.model;

import java.util.ArrayList;

public class Car implements Identifiable {
    private final ArrayList<Detail> details;
    private final String model;
    private long id;

    public Car(String model, Detail body, Detail engine, Detail accessory) {
        this.model = model;
        this.details = new ArrayList<>(3);
        this.details.add(body);
        this.details.add(engine);
        this.details.add(accessory);
    }

    public String getModel() {
        return model;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Auto <" + id + "> " +
                "(Body: <" + details.get(0).getId() + "> " +
                "Engine: <" + details.get(1).getId() + "> " +
                "Accessory: <" + details.get(2).getId() + ">)";
    }
}
