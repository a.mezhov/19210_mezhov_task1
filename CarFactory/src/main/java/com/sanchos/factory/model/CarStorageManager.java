package com.sanchos.factory.model;

public class CarStorageManager {
    public final int MIN_CAR;

    private final CarFactory mechanics;
    private final Storage<Car> carStorage;
    private int soldCarCounter = 0;
    private final boolean isLogging;
    private int qtyRequested = 0; // количество запросов

    public CarStorageManager(CarFactory mechanics, Storage<Car> carStorage, boolean isLogging) {
        MIN_CAR = (int) (carStorage.getCapacity() * 0.1);
        this.mechanics = mechanics;
        this.carStorage = carStorage;
        this.isLogging = isLogging;
    }

    synchronized public void requestCar() {
        mechanics.createCar();
        qtyRequested++;
    }

    synchronized public void fillMinimum() {
        if (qtyRequested < MIN_CAR && carStorage.getNumberItems() < MIN_CAR) {
            for (int i = 0; i < MIN_CAR - carStorage.getNumberItems(); i++) {
                mechanics.createCar();
                qtyRequested++;
            }
        }
    }

    synchronized public void notifyAboutReceiving(String message) { // уведомление о том, что машина была собрана
        if (isLogging) {
            System.out.println(message);
        }
        soldCarCounter++;
        qtyRequested--;
    }

    public int getSoldCarCounter() {
        return soldCarCounter;
    }

}
