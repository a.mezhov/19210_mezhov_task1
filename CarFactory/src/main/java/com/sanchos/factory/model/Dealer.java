package com.sanchos.factory.model;

import java.util.concurrent.TimeUnit;

public class Dealer implements Runnable {
    private final CarStorageManager manager;
    private final Storage<Car> carStorage;
    private int periodRequest = 1500; // период запроса равен 1.5 сек

    public Dealer(Storage<Car> carStorage, CarStorageManager manager) {
        this.manager = manager;
        this.carStorage = carStorage;
    }

    public Car requestCar() throws InterruptedException {
        manager.requestCar(); // запрос на сборку машины
        return carStorage.getItem();
    }

    public void setPeriodRequest(int period) {
        this.periodRequest = period;
    }

    public int getPeriodRequest() {
        return periodRequest;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Car car = requestCar();
                manager.notifyAboutReceiving(car.toString() + " Dealer <" + Thread.currentThread().getName() + "> ");
                TimeUnit.MILLISECONDS.sleep(periodRequest);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
