package com.sanchos.factory.model;

public enum TypesDetail {
    BODY,
    ENGINE,
    ACCESSORY
}
