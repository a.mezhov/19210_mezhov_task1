package com.sanchos.factory.model;

public class Detail implements Identifiable{
    private final TypesDetail detail;
    private long id;

    public Detail(TypesDetail detail){
        this.detail = detail;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getId(){
        return id;
    }
}
