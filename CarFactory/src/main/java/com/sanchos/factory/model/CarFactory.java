package com.sanchos.factory.model;

import com.sanchos.threadpool.ThreadPool;

public class CarFactory {
    public static final int WORK_TIME = 1000; // задержка одна секунда

    private final ThreadPool threadPool;
    private final Storage<Detail> accessoryStorage;
    private final Storage<Detail> bodyStorage;
    private final Storage<Detail> engineStorage;
    private final Storage<Car> carStorage;

    public CarFactory(int qtyMechanics, Storage<Detail> accessory, Storage<Detail> body, Storage<Detail> engine, Storage<Car> car) {
        this.threadPool = new ThreadPool(qtyMechanics);
        this.accessoryStorage = accessory;
        this.bodyStorage = body;
        this.engineStorage = engine;
        this.carStorage = car;
    }

    public void createCar() {
        Runnable task = () -> { // описание задачи для потока
            try { // достаём детали из склада
                Detail body = bodyStorage.getItem();
                Detail engine = engineStorage.getItem();
                Detail accessory = accessoryStorage.getItem();

                Thread.sleep(WORK_TIME);

                Car car = new Car("BMW", body, engine, accessory);
                carStorage.addItem(car); // добавляем машину на склад машин
            } catch (InterruptedException ignored) {
            }
        };
        threadPool.execute(task);
    }

    public void stop() {
        threadPool.shutdown();
    }

}
