package com.sanchos.factory.model;

public interface Identifiable {
    void setId(long id);
    long getId();
}
