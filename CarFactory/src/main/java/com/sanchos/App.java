package com.sanchos;

import com.sanchos.factory.ResourceController;

public class App {
    public static void main(String[] args) {
        ResourceController controller = new ResourceController();
        controller.start();
    }
}
