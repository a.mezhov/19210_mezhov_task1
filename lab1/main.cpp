#include "TritSet.h"
#include "gtest/gtest.h"
#include "iostream"

TEST(Tests, _and) {
TritSet a(7);
a[0] = True;
a[1] = False;
a[3] = True;
a[4] = False;
a[5] = True;
TritSet b(50);
b[0] = False;
b[1] = False;
b[2] = False;
b[3] = True;
b[4] = True;
b[30] = True;
b[41] = False;
TritSet c = a & b;
EXPECT_EQ(b.capacity(), c.capacity());
EXPECT_EQ(c[0], False);
EXPECT_EQ(c[1], False);
EXPECT_EQ(c[2], False);
EXPECT_EQ(c[3], True);
EXPECT_EQ(c[4], False);
EXPECT_EQ(c[5], Unknown);
EXPECT_EQ(c[6], Unknown);
EXPECT_EQ(c[20], Unknown);
EXPECT_EQ(c[30], Unknown);
EXPECT_EQ(c[41], False);
}

TEST(Tests, _or) {
TritSet a(10);
a[0] = True;
a[1] = False;
a[3] = True;
a[4] = False;
a[5] = True;
TritSet b(50);
b[0] = False;
b[1] = False;
b[2] = False;
b[3] = True;
b[4] = True;
b[20] = True;
TritSet c = b | a;
EXPECT_EQ(b.capacity(), c.capacity());
EXPECT_EQ(c[0], True);
EXPECT_EQ(c[1], False);
EXPECT_EQ(c[2], Unknown);
EXPECT_EQ(c[3], True);
EXPECT_EQ(c[4], True);
EXPECT_EQ(c[5], True);
EXPECT_EQ(c[6], Unknown);
EXPECT_EQ(c[20], True);
EXPECT_EQ(c[21], Unknown);
EXPECT_EQ(c[22], Unknown);
}


TEST(Tests, _not) {
TritSet a(4);
a[0] = True;
a[1] = False;
a = ~a;
EXPECT_EQ(a[0], False);
EXPECT_EQ(a[1], True);
EXPECT_EQ(a[3], Unknown);
}

TEST(Tests, _trim) {
TritSet a(100);
int capacity = a.capacity();
EXPECT_EQ(a.length(), 0);
a[10000000] = False;
EXPECT_EQ(a.length(), 10000001);
a.trim(100);
EXPECT_EQ(a.length(), 0);
EXPECT_EQ(a.capacity(), capacity);
}

TEST(Tests, _capasity) {
TritSet set (1000);
int capacity = set.capacity();
set[0] = True;
set[10000] = False;
EXPECT_GE(capacity, 1000 * 2 / 8 / sizeof(uint));
EXPECT_EQ(set[0], True);
EXPECT_EQ(set[10000], False);
EXPECT_EQ(set[100], Unknown);
capacity = set.capacity();
EXPECT_EQ(set.capacity(), capacity);
}

TEST(Tests, _shrink) {
TritSet set (10);
int capacity = set.capacity();
set[100] = True;
set[100] = Unknown;
EXPECT_GT(set.capacity(), capacity);
set.shrink();
EXPECT_EQ(set.capacity(), capacity);
set[100] = True;
capacity = set.capacity();
set[1000] = False;
EXPECT_GT(set.capacity(), capacity);
set[1000] = Unknown;
set.shrink();
EXPECT_EQ(set.capacity(), capacity);
}

TEST(Tests, _cardinality) {
TritSet a(10);
a[0] = True;
a[1] = False;
a[3] = True;
a[5] = True;
EXPECT_EQ(a.cardinality(True), 3);
EXPECT_EQ(a.cardinality(False), 1);
EXPECT_EQ(a.cardinality(Unknown), 6);
EXPECT_EQ(a.cardinality()[True], a.cardinality(True));
EXPECT_EQ(a.cardinality()[False], a.cardinality(False));
EXPECT_EQ(a.cardinality()[Unknown], a.cardinality(Unknown));
}

int main () {
    //testing::InitGoogleTest(&argc, argv);
    //return RUN_ALL_TESTS();

    /// конструктор копирования работает:
    TritSet a(1);
    a[0] = True;
    TritSet(b) = a;
    b[0] = False;
    std::cout << a << ' ' << b;



    /// перегрузка оператора вывода:
    TritSet set(100);
    cout << set << '\n';

    for(int i = 0; i < 50; i++)
        set[i] = False;
    for(int i = 51; i < 100; i++)
        set[i] = Unknown;

    cout << set << '\n';

    return 0;
}
