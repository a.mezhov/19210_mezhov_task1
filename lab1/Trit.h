#ifndef TRIT_HEADER
#define TRIT_HEADER
#include <iostream>

enum Trit{False, Unknown, True};

const std::ostream& operator<<(std::ostream& out, const Trit &trit);

const Trit operator~ (Trit trit);

const Trit operator& (Trit trit1, Trit trit2);

const Trit operator| (Trit trit1, Trit trit2);

#endif


