#include "TritSet.h"

#define TRUE 3
#define FALSE 2
#define UNKNOWN 0

TritSet::TritProxy::TritProxy(TritSet *Mas, int _index) : mas(*Mas), index(_index) {}

TritSet::TritProxy &TritSet::TritProxy::operator=(Trit val) {
    if(mas.array.size() < ceil((double)(index + 1) / (4 * sizeof(uint))) && val != Unknown) {
        mas.array.resize(ceil((double)(index + 1) / (4 * sizeof(uint))), 0);
        mas.countTrits = index + 1;
    } else if(index >= mas.countTrits && val != Unknown) {
        mas.countTrits = index + 1;
    }
    mas.valueInMas(index, val);
    return *this;
}

bool TritSet::TritProxy::operator==(const TritProxy &a) const {
    if (index < mas.countTrits && a.index < a.mas.countTrits) {
        return (mas.value(index) == a.mas.value(a.index));
    }
    else if (index >= mas.countTrits && a.mas.value(a.index) == Unknown ||
             a.index >= a.mas.countTrits && mas.value(index) == Unknown) {
        return true;
    }
    return false;
}

/// при обращении к неустановленным тритам чтение должно возвращать значение Unknown:
bool TritSet::TritProxy::operator==(const Trit &a) const {
    if (index > mas.countTrits) {
        return a == Unknown;
    }
    return (mas.value(index) == a);
}

TritSet::TritProxy::operator Trit() {
    return this->mas.value(this->index);
}

void TritSet::valueInMas(int index, Trit val) { ///запись трита в массив
    if(value(index) == True) {
        countTrue--;
    } else if(value(index) == False) {
        countFalse--;
    }

    if(val == True) {
        countTrue++;
    } else if(val == False) {
        countFalse++;
    }
    int point1 = index / (4 * sizeof(uint));
    int point2 = index % (4 * sizeof(uint));
    uint a = 3;
    uint b;
    if(val == False) {
        b = FALSE;
    } else if(val == True) {
        b = TRUE;
    } else {
        b = UNKNOWN;
    }
    a <<= 2 * point2;
    b <<= 2 * point2;
    a = ~a;
    array[point1] &= a;
    array[point1] |= b;
}

TritSet::TritSet(int size) {
    array.assign(ceil((double) size / (4 * sizeof(uint))), 0);
    initialSize = array.size(); /// текущий размер = размер массива, который нам подали на входе
    countTrits = size;
    countTrue = 0;
    countFalse = 0;
}

TritSet::TritSet(const TritSet& other){
    this->initialSize = other.initialSize;
    this->countTrits = other.countTrits;
    this->countTrue = other.countTrue;
    this->countFalse = other.countFalse;
};

TritSet::TritSet() {
    initialSize = 0;
    countTrits = 0;
    countTrue = 0;
    countFalse = 0;
}

TritSet::~TritSet() { /// очистка вектора
    array.clear();
}

size_t TritSet::size() const {
    return countTrits;
}

size_t TritSet::arraySize() const {
    return array.size();
}

uint TritSet::valueCell(int index) const {
    if (index >= array.size()) {
        return 0;
    }
    return array[index];
}

size_t TritSet::capacity() const {
    return array.capacity();
}

Trit TritSet::value(int index) const {
    if(index > countTrits) {
        return Unknown;
    }
    int point1 = index / (4 * sizeof(uint));  ///ячейке массива, в которой находится трит
    int point2 = index % (4 * sizeof(uint)); ///на каком месте в этой ячейке находится трит
    uint a = array[point1];
    a >>= 2 * point2;
    a &= 3;
    if(a == FALSE) {
        return False;
    } else if(a == TRUE) {
        return True;
    } else {
        return Unknown;
    }
}

void TritSet::shrink() {
    int significant_size = initialSize;
    for (int k = array.size() - 1; k > initialSize; --k) {
        if (array[k] > 0) {
            significant_size = k + 1;
            break;
        }
    }
    array.resize(significant_size);
    int point1 = 4 * sizeof(uint) * (significant_size - 1);
    int point2 = 4 * sizeof(uint) * significant_size;
    for (int i = point1; i < point2; ++i) {
        if (value(i) != Unknown) {
            countTrits = i + 1;
        }
    }
    array.shrink_to_fit(); /// уменьшает емкость контейнера до его размера и уничтожает все элементы за пределами емкости.
}

size_t TritSet::cardinality(Trit value) {
    if(value == True) {
        return countTrue;
    } else if(value == False) {
        return countFalse;
    } else {
        return (countTrits - countTrue - countFalse);
    }
}

std::unordered_map <Trit, int, std::hash<int>> TritSet::cardinality() {
    std::unordered_map <Trit, int, std::hash<int>> count;
    count[True] = cardinality(True);
    count[False] = cardinality(False);
    count[Unknown] = cardinality(Unknown);
    return count;
}

void TritSet::trim(size_t lastIndex) {
    if (lastIndex >= countTrits) {
        return;
    }
    int significant_size = ceil((double)lastIndex / (4 * sizeof(uint)));
    int point = significant_size * 4 * sizeof(uint);
    for (int i = lastIndex; i < point; ++i) {
        valueInMas(i, Unknown);
    }
    array.resize(significant_size);
    countTrits = lastIndex + 1;
    array.shrink_to_fit();
}

size_t TritSet::length() const {
    int lastIndex = -1;
    for (int i = array.size() - 1; i >= 0; --i) {
        if (array[i] != 0) {
            lastIndex = i;
            break;
        }
    }
    if (lastIndex == -1) {
        return 0;
    }
    int point1 = 4 * sizeof(uint) * lastIndex;
    int point2 = 4 * sizeof(uint) * (lastIndex + 1);
    for (int i = point2 + 1; i >= point1; --i) {
        if (value(i) != Unknown) {
            return i + 1;
        }
    }
    return 0;
}

TritSet::TritProxy TritSet::operator[] (int index) {
    return TritProxy(this, index);
}

TritSet& TritSet::operator=(const TritSet &a) {
    array = a.array;
    countTrits = a.countTrits;
    countTrue = a.countTrue;
    countFalse = a.countFalse;
    return *this;
}

TritSet operator~ (const TritSet &a) {
    TritSet result(a.size());
    for (int i = 0; i < a.arraySize(); ++i) {
        if (a.valueCell(i) == 0) continue;
        int point1 = i * 4 * sizeof(uint);
        int point2 = (i + 1) * 4 * sizeof(uint);
        for (int j = point1; j < point2; ++j) {
            result[j] = ~a.value(j);
        }
    }
    return result;
}

TritSet operator& (const TritSet &a, const TritSet &b) {
    TritSet result(std::max(a.size(), b.size()));
    for (int i = 0; i < std::max (a.arraySize(), b.arraySize()); ++i) {
        if (a.valueCell(i) == 0 && b.valueCell(i) == 0) {
            continue;
        }
        int point1 = i * 4 * sizeof(uint);
        int point2 = (i + 1) * 4 * sizeof(uint);
        for (int j = point1; j < point2; ++j) {
            result[j] = a.value(j) & b.value(j);
        }
    }
    return result;
}

TritSet operator| (const TritSet &a, const TritSet &b) {
    TritSet result(std::max(a.size(), b.size()));
    for (int i = 0; i < std::max (a.arraySize(), b.arraySize()); ++i) {
        if (a.valueCell(i) == 0 && b.valueCell(i) == 0) {
            continue;
        }
        int point1 = i * 4 * sizeof(uint);
        int point2 = (i + 1) * 4 * sizeof(uint);
        for (int j = point1; j < point2; ++j) {
            result[j] = a.value(j) | b.value(j);
        }
    }
    return result;
}

ostream& operator <<(std::ostream& stream, TritSet& set){
    for(int i = 0; i < set.length(); ++i){
        stream << Trit(set[i]);
        std::cout << ' ';
    }
    return stream;
}

