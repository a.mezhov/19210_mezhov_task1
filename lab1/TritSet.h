#include "Trit.h"
#include <iostream>
#include <vector>
#include <unordered_map>
#include <cmath>
#include <algorithm>

using namespace std;

typedef unsigned int uint;

class TritSet {
private:
    std::vector <uint> array;
    size_t initialSize, countTrits, countTrue, countFalse;
    /// a[i] = B;
    class TritProxy {
        friend class TritSet;
    private:
        TritSet &mas;
        int index;
    public:
        TritProxy(TritSet *Mas, int _index); /// конструктор

        TritProxy &operator=(Trit val);

        bool operator==(const TritProxy &a) const;

        bool operator==(const Trit &a) const;

        explicit operator Trit();
    };

    void valueInMas(int index, Trit val); /// запись трита в массив
public:
    explicit TritSet(int size); /// конструктор, если нам подали размер массива

    TritSet(const TritSet& a); /// конструктор копирования

    TritSet(); /// конструктор, если нам не подают размер массива

    ~TritSet();

    size_t size() const; /// возвращает количество тритов

    size_t arraySize() const; /// размер вектора

    uint valueCell(int index) const; /// значение ячейки

    size_t capacity() const; /// ёмкость вектора

    Trit value(int index) const; /// достаём значение трита по индексу ячейки в векторе

    void shrink(); /// подчищение векторара от пустых ячеек и "пустых" значений тритов

    size_t cardinality(Trit value); /// число установленных в данное значение тритов
/// для трита Unknown - число значений Unknown до последнего установленного трита


    std::unordered_map <Trit, int, std::hash<int>> cardinality(); /// число установленных для всех типов тритов

    void trim(size_t lastIndex);  /// забыть содержимое от lastIndex и дальше

    size_t length() const; /// индекс последнего не Unknown трита + 1

    TritProxy operator[] (int index);

    TritSet &operator=(const TritSet &a);

    friend std::ostream& operator<<(std::ostream& out, TritSet &set);
};

TritSet operator~ (const TritSet &a);

TritSet operator& (const TritSet &a, const TritSet &b);

TritSet operator| (const TritSet &a, const TritSet &b);





