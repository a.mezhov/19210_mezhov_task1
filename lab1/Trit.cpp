#include "Trit.h"
#define TRUE 3
#define FALSE 2
#define UNKNOWN 0
/// логика тритов(таблица истинности)

const std::ostream& operator<<(std::ostream& out, const Trit &trit) {
    if(trit == True) {
        out << "True";
    } else if(trit == False) {
        out << "False";
    } else {
        out << "Unknown";
    }
    return out;
}

const Trit operator~ (Trit trit) {
    if(trit == True) {
        return False;
    } else if(trit == False) {
        return True;
    } else {
        return Unknown;
    }
}

const Trit operator& (Trit trit1, Trit trit2) {
    if(trit1 == False) {
        return False;
    } else if(trit1 == Unknown) {
        if(trit2 == False) {
            return False;
        } else {
            return Unknown;
        }
    } else {
        return trit2;
    }
}

const Trit operator| (Trit trit1, Trit trit2) {
    if(trit1 == True) {
        return True;
    } else if(trit1 == False) {
        return trit2;
    } else {
        if(trit2 == True) {
            return True;
        } else {
            return Unknown;
        }
    }
}